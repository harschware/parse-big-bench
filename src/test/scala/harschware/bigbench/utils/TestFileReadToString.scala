package harschware.bigbench.utils

import org.junit.Assert.assertTrue
import org.junit.Test
import org.slf4j.LoggerFactory

import harschware.sandbox.JavaFileUtils

@Test
class TestFileReadToString {
  var LOGGER = LoggerFactory
    .getLogger(classOf[TestFileReadToString]);

  val fileName = "FileUtils-test.txt"
  val testFileUrl = Thread.currentThread()
    .getContextClassLoader().getResource(fileName)

  @Test
  def testResourceLoading() = {
    assertTrue(getClass.getClassLoader().getResourceAsStream(fileName) != null)
    assertTrue(getClass.getClassLoader().getResourceAsStream("/" + fileName) == null)
    assertTrue(getClass.getResourceAsStream(fileName) == null)
    assertTrue(getClass.getResourceAsStream("/" + fileName) != null)
    assertTrue(classOf[TestFileReadToString].getResourceAsStream(fileName) == null)
    assertTrue(classOf[TestFileReadToString].getResourceAsStream("/" + fileName) != null)
  }

  @Test
  def testJava() = {
    val testFileContent = JavaFileUtils.readFile(testFileUrl.getPath());
    LOGGER.info(testFileContent)

    assertTrue(testFileContent.equals("test"));
  }

  @Test
  def testScala() = {
    val testFileContent = io.Source.fromInputStream(getClass.getResourceAsStream("/" + fileName)).mkString
    LOGGER.info(testFileContent)
    assertTrue(testFileContent.equals("test"));
  }
}


