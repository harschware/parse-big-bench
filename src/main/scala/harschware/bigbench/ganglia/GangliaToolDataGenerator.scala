package harschware.bigbench.ganglia

import java.io.File
import scala.io.Source
import scala.collection.mutable.StringBuilder
import org.joda.time.DateTime
import com.github.nscala_time.time.Imports.richReadableInstant
import harschware.bigbench.utils.BigBenchLogUtils
import harschware.bigbench.utils.DateUtils

object GangliaToolDataGenerator {

  /*
   * event	SomeEvent	1/9/2015 9:30
event	SomeIntervalEvent	1/9/2015 10:30	1/9/2015 11:30
host	localhost
chart	load_one	1/6/2015 16:42	1/6/2015 20:32
chart	load_one	01/6/2015 16:42	1/06/2015 20:32
chart	load_one	2015-01-06T16:42	2015-01-06T20:32
   * 
   */
  def genGangliaInputFile(times: Map[String, (DateTime, DateTime)], gangliaNodes: File, gangliaChartTypes: File): String = {
    val sb = new StringBuilder()
    var minStart = DateTime.now()
    var maxFinish = new DateTime(0) // 0 is epoch

    val sortedKeys = times.keys.toSeq.sortBy(key => key)
    for (k <- sortedKeys) {
      val v = times(k)
      if (v._1 < minStart)
        minStart = v._1
      if (v._2 > maxFinish)
        maxFinish = v._2
        
      // for the moment... no events
      // if (k.startsWith("P")) sb.append(s"event\t${k}\t" + DateUtils.DATE_FORMATTER_ISO.print(v._1) + "\t" + DateUtils.DATE_FORMATTER_ISO.print(v._2) + "\n")
    } // end for

    val hosts = if (gangliaNodes == null)
      List("cluster")
    else
      Source.fromFile(gangliaNodes, "UTF-8").getLines().toSeq
    val chartTypes = if (gangliaChartTypes == null)
      List("load_all_report", "mem_report", "network_report")
    else
      Source.fromFile(gangliaChartTypes, "UTF-8").getLines().toSeq

    for (resource <- hosts) {
      if (!resource.equalsIgnoreCase("cluster")) {
        sb.append(s"host\t${resource}\n")
      } // end if
      for (chartType <- chartTypes) {
        sb.append(s"chart\t${chartType}\t" + DateUtils.DATE_FORMATTER_ISO.print(minStart) + "\t" + DateUtils.DATE_FORMATTER_ISO.print(maxFinish) + "\n")
      } // end for
    } // end for

    return sb.toString
  } // end function

  def getStartEndTimesFromCsv(logPath: File, fileLabels: Map[String, String]): Map[String, (DateTime, DateTime)] = {
    val lines = Source.fromFile(logPath + File.separator + "BigBenchTimes.csv").getLines
    lines.drop(1)

    val logsAndDates = scala.collection.mutable.Map[String, (DateTime, DateTime)]();

    for (line <- lines) {
      // println(line)
      val flds = line.split(';')
      val lbl = BigBenchLogUtils.testNameToShortLabel(flds(1), flds(2), flds(3)).
        replaceAll("Power Test", "P_").
        replaceAll("Throughput 1st run", "T1_").
        replaceAll("Throughput 2nd run", "T2_").
        replaceAll("\\s+", "")

      if (flds(2).nonEmpty && flds(3).nonEmpty) {
        val startTimeStr = flds(4)
        val endTimeStr = flds(5)
        // println(lbl + "\t" + startTimeStr + "\t" + endTimeStr)
        val startTime = new DateTime(startTimeStr.toLong)
        val endTime = new DateTime(endTimeStr.toLong)
        logsAndDates.put(lbl, (startTime, endTime))
      } // end if
    } // end for

    return logsAndDates.toMap
  } // end function

} // end object